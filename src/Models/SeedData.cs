using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using program.Data;

namespace program.Models
{
    public static class SeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new programContext(
                        serviceProvider.GetRequiredService<
                        DbContextOptions<programContext>>()))
            {
                if (context.Book.Any())
                {
                    return; // DB has been seeded. No action.
                }
                context.Book.AddRange(
                    new Book()
                    {
                        Title = "Defensive Programming",
                        PrintDate = DateTime.Parse("2021-1-1"),
                        Price = 100,
                        isReviewed = true
                    },
                    new Book()
                    {
                        Title = "Secure Programming",
                        PrintDate = DateTime.Parse("2020-1-1"),
                        Price = 99,
                        isReviewed = true
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
