using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using program.Models;
using program.Data;

namespace program.Tests
{
    [TestFixture, Category("security")]
    public class programSecurityTest
    {
        [Test]
        public void should_Not_CreateBook_When_Title_overLong()
        {
            // Arrange
            var book = new Book 
            { 
                Title = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA",
                PrintDate = DateTime.Now,
                Price = 100,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("Title")));
        }

        [Test]
        public void should_Not_CreateBook_When_Title_tooShort()
        {
            // Arrange
            var book = new Book 
            { 
                Title = "AA",
                PrintDate = DateTime.Now,
                Price = 100,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("Title")));
        }

        [Test]
        public void should_Not_CreateBook_When_Date_tooOld()
        {
            // Arrange
            var book = new Book 
            { 
                Title = "ATTACK",
                PrintDate = new DateTime(1900, 01, 01),
                Price = 100,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("PrintDate")));
        }

        [Test]
        public void should_Not_CreateBook_When_Date_tooFuture()
        {
            // Arrange
            var book = new Book 
            { 
                Title = "ATTACK",
                PrintDate = new DateTime(2120, 01, 01),
                Price = 100,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("PrintDate")));
        }

        [Test]
        public void should_Not_CreateBook_When_Price_tooHigh()
        {
            // Arrange
            var book = new Book 
            { 
                Title = "ATTACK",
                PrintDate = DateTime.Now,
                Price = 9999999999999999999,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("Price")));
        }

        [Test]
        public void should_Not_CreateBook_When_Price_tooLow()
        {
            // Arrange
            var book = new Book 
            { 
                Title = "ATTACK",
                PrintDate = DateTime.Now,
                Price = 01,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("Price")));
        }

        [Test]
        public void should_Not_CreateBook_When_isReview_provided()
        {
            // Arrange & Act
            var book = new Book 
            { 
                Title = "ATTACK",
                PrintDate = DateTime.Now,
                Price = 100,
                isReviewed = true,
            };
            // Assert
            Assert.IsTrue(book.isReviewed == false);
        }

        private IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }
    }

    
}
