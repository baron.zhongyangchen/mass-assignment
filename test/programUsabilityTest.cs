using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using program.Models;
using program.Data;

namespace program.Tests
{
    [TestFixture, Category("usability")]
    public class programUsabilityTest
    {
        [Test]
        public async Task should_createbook_when_validInput()
        {
            using(var db = new programContext(Utilities.TestDbContextOptions()))
            {
                // Arrange
                var expectedData = new Book 
                { 
                    ID = 1337,
                    Title = "Test",
                    PrintDate = DateTime.Now,
                    Price = 100,
                };
                // Act
                await db.AddBookAsync(expectedData);
                // Assert
                var actualData = await db.FindAsync<Book>(1337);
                Assert.AreEqual(expectedData, actualData);

            }
        }
        
        [Test]
        public void should_not_createBook_when_titleMissing()
        {
            // Arrange
            var book = new Book 
            { 
                Title = null,
                PrintDate = DateTime.Now,
                Price = 100,
            };
            // Act
            var results = ValidateModel(book);
            // Assert
            Assert.IsTrue(results.Any(v => v.MemberNames.Contains("Title")));
            Assert.IsTrue(results.Any(v => v.ErrorMessage.Contains("required")));
        }

        private IList<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }
    }

    
}
